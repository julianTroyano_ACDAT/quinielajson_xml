package com.jtcode.quiniela;

import android.app.ProgressDialog;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    EditText edtlocal,edtResultado,edtApuesta;
    RadioGroup rdg;
    RadioButton rdbJson;
    Button btn;

    String extension;
    String fich;
    String[] apuestas;
    static String[] resultados;
    boolean ambosFicheros;

    File resultadoPremios;

    File ficheroxml;
    File ficheroApuestas;
    File ficheroPremios;

    JSONObject quinielajson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            init();
        }catch(Exception e){
            Toast.makeText(MainActivity.this, "ha ocurrido un error inesperado", Toast.LENGTH_SHORT).show();
        }
    }
    private void init(){
        rdbJson=(RadioButton)findViewById(R.id.radioJson);
        edtlocal=(EditText)findViewById(R.id.edtLocalPremios);
        edtApuesta=(EditText)findViewById(R.id.edtApuestas);
        edtResultado=(EditText)findViewById(R.id.edtResultados);
        btn=(Button)findViewById(R.id.button);
        rdg=(RadioGroup)findViewById(R.id.radioGroup);
        ambosFicheros=false;

        rdg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                cambioExt();
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtlocal.getText().toString().trim().length() == 0 ||
                        edtResultado.getText().toString().trim().length() == 0 ||
                        edtApuesta.getText().toString().trim().length()== 0) {

                    Toast.makeText(MainActivity.this, "Faltan datos", Toast.LENGTH_SHORT).show();

                } else {
                    if (comprobacionMemoriaExterna()) {
                        if (!isNetworkAvailable()) {
                            Toast.makeText(MainActivity.this, "No hay conexion, no se puede descargar", Toast.LENGTH_SHORT).show();
                        } else {
                            fich=edtlocal.getText().toString();
                            //descarga
                            try {
                                Analisis.limpieza();
                                cambioExt();
                                //comprobacion del final del archivo para que concuerde con la extension
                               descarga(edtResultado.getText().toString(),edtApuesta.getText().toString(),rdbJson.isChecked());

                            } catch (IOException e) {
                                e.printStackTrace();

                            }
                        }
                    } else {
                        Toast.makeText(MainActivity.this, "No hay memoria externa disponible", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    //region comprobaciones sd y conexion

    private boolean comprobacionMemoriaExterna() {
        String estado = Environment.getExternalStorageState();

        return estado.equals(Environment.MEDIA_MOUNTED);
    }

    private boolean isNetworkAvailable(){
        boolean result = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            result = true;

        return result;
    }

    //endregion

    private void cambioExt() {
        if (rdbJson.isChecked()) {
           extension= ".json";
        } else {
            extension = ".xml";
        }
        if (edtlocal.getText().toString().contains(".") && edtlocal.getText().toString().substring(0, edtlocal.getText().toString().indexOf(".")).trim().length() != 0) {
            edtlocal.setText(edtlocal.getText().toString().substring(0, edtlocal.getText().toString().indexOf(".")) + extension);
        } else if (edtlocal.getText().toString().trim().length() != 0) {
            edtlocal.setText(edtlocal.getText().toString().concat(extension));
        } else {
            edtlocal.setText("premios" + extension);
        }

    }

    private void acabadaDescargas(boolean json){
        try {
            if(ambosFicheros) {
                ambosFicheros=!ambosFicheros;
                if (json) {
                    asignarResultados(Analisis.obtenerResultadoFromJSON(quinielajson));
                } else {
                    asignarResultados(Analisis.obtenerResultadoFromXML(ficheroxml));
                }
                Analisis.analisisResultados(ficheroApuestas, ficheroPremios, !json, MainActivity.resultados);

                Toast.makeText(MainActivity.this, "Archivos descargados y comprobados los resultados", Toast.LENGTH_SHORT).show();
                subida(ficheroPremios);

            }else{ambosFicheros=!ambosFicheros;}

        } catch (IOException e) {
            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (XmlPullParserException e) {
            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (ExcepcionApuestas excepcionApuestas) {
            Toast.makeText(MainActivity.this, excepcionApuestas.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (ExcepcionEscritura excepcionEscritura) {
            Toast.makeText(MainActivity.this, excepcionEscritura.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (ExceotionFicherosDescargados e){
            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    private void descarga(String urlResultado, String urlApuesta, final boolean json) throws IOException {

        ficheroxml= new File(Constantes.LOCALSDDIR.concat("/"+fich));
        ficheroApuestas=new File(Constantes.LOCALSDDIR.concat("/apuestas.txt"));
        ficheroPremios=new File(Constantes.LOCALSDDIR.concat("/premios"+extension));

        final RequestQueue requestQueue= Volley.newRequestQueue(MainActivity.this);

        final ProgressDialog progressDialog= new ProgressDialog(MainActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Descargando...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        if(!json) {
            //peticion para los resultados xml
           /* StringRequest respuestaResultados = new StringRequest(Request.Method.GET, urlResultado, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        BufferedWriter br= new BufferedWriter(new OutputStreamWriter(new FileOutputStream(ficheroxml,false)));
                        br.write(response);
                        br.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            requestQueue.add(respuestaResultados);
            */
            AsyncHttpClient client=new AsyncHttpClient();

           client.get(urlResultado, new FileAsyncHttpResponseHandler(ficheroxml) {
               @Override
               public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                   progressDialog.dismiss();
                   Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
               }

               @Override
               public void onSuccess(int statusCode, Header[] headers, File file) {
                   Toast.makeText(MainActivity.this, "Fichero descargado", Toast.LENGTH_SHORT).show();
                    acabadaDescargas(json);
               }
           });

        }else {
            //peticion para los resultados JSON
            JsonObjectRequest jsonrequest= new JsonObjectRequest(Request.Method.GET, urlResultado, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONObject jsonfile= response.getJSONObject("quinielista");
                        quinielajson=jsonfile;
                    } catch (JSONException e) {
                        Toast.makeText(MainActivity.this,"error en el fichero json",Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(MainActivity.this, "Se produjo un error: "+error.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
            requestQueue.add(jsonrequest);
        }

        //apuestas
        StringRequest respuestaApuestas= new StringRequest(Request.Method.GET, urlApuesta, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                apuestas=response.split("\n");
                BufferedWriter br= new BufferedWriter(new OutputStreamWriter(new FileOutputStream(ficheroApuestas,false)));

                    for(String s: apuestas) {
                        br.write(s);
                        br.newLine();
                    }

                br.close();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                progressDialog.dismiss();
                acabadaDescargas(json);
            }
        });
        //cola
        requestQueue.add(respuestaApuestas);


    }

    private static void asignarResultados(String[] resultado) throws ExceotionFicherosDescargados {
        if(resultado.length==0 || resultado!=null)
        resultados=resultado;
        else
            throw new ExceotionFicherosDescargados("error en los ficheros descargados");
    }

    private void subida(File fichero){
        final ProgressDialog progressDialog= new ProgressDialog(MainActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Subiendo premios...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        RequestParams params= new RequestParams();

        try {

            params.put("premios",fichero);
            RestClient.post(Constantes.URLSERVGUARDAR, params, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "no se pudo subir al servidor los resultados", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    progressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Se han subido los resultados de los premiados", Toast.LENGTH_SHORT).show();
                    ambosFicheros=false;
                    resultados=null;
                }
            });

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            progressDialog.dismiss();
        }
    }

}
