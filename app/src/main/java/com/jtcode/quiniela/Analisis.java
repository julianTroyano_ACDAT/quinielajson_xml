package com.jtcode.quiniela;

import android.util.Log;
import android.util.Xml;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class Analisis {

    private static List<Apuesta> apuestasPremiadas= new ArrayList<Apuesta>();//lista de las apuestas premiadas que se añadirán al archivo
    private static Resultado result;//resultados

    static String el10,el11,el12,el13,el14,el15;

    public static void limpieza(){
        apuestasPremiadas=null;
        result=null;
        apuestasPremiadas=new ArrayList<>();
    }

    public static String[] obtenerResultadoFromXML(File fichero) throws IOException, XmlPullParserException {

        boolean dentroquiniela=false;

        //lectura del fichero xml de los resultados
        String[] resultado=new String[15];

        int cont=0;

        XmlPullParser xpp= Xml.newPullParser();
        xpp.setInput(new FileReader(fichero));
        int eventType=xpp.getEventType();
        int i=0;
        while (eventType!=XmlPullParser.END_DOCUMENT){

            switch (eventType){
                case XmlPullParser.START_TAG:
                    if(xpp.getName().equalsIgnoreCase("quiniela")) {
                        dentroquiniela = true;

                        if (!(xpp.getAttributeValue(3)+xpp.getAttributeValue(4)+xpp.getAttributeValue(5)+ xpp.getAttributeValue(6)+
                                xpp.getAttributeValue(7)+ xpp.getAttributeValue(8)).equalsIgnoreCase("000000")) {

                            i = Integer.parseInt(xpp.getAttributeValue(0));
                            el10 = xpp.getAttributeValue(3);
                            el11 = xpp.getAttributeValue(4);
                            el12 = xpp.getAttributeValue(5);
                            el13 = xpp.getAttributeValue(6);
                            el14 = xpp.getAttributeValue(7);
                            el15 = xpp.getAttributeValue(8);
                        }
                        else{
                            return resultado;
                        }
                    }
                    if(xpp.getName().equalsIgnoreCase("partit") && dentroquiniela){
                           resultado[cont]=xpp.getAttributeValue(xpp.getAttributeCount() - 1);
                           cont=(cont+1)%resultado.length;
                    }

                    break;

                case XmlPullParser.END_TAG:
                    if (xpp.getName().equals("quiniela"))
                        dentroquiniela = false;
                    break;
            }
            eventType=xpp.next();
        }

        return null;
    }

    public static String[] obtenerResultadoFromJSON(JSONObject fichero) throws JSONException, ExcepcionApuestas {
        //lectura del fichero json de los resultados
        try {
            String[] resultado = new String[15];

            JSONArray quiniela = fichero.getJSONArray("quiniela");
            int cont = 0;

            for (int i = 0; i < 30; i++) {
                String eldebug = (quiniela.getJSONObject(3).getString("el10") + quiniela.getJSONObject(4).getString("el11") + quiniela.getJSONObject(5).getString("el12") +
                        quiniela.getJSONObject(6).getString("el13") + quiniela.getJSONObject(7).getString("el14") + quiniela.getJSONObject(8).getString("el15"));
                if (!eldebug.equalsIgnoreCase("000000")) {

                    el10 = quiniela.getJSONObject(3).getString("el10");
                    el11 = quiniela.getJSONObject(4).getString("el11");
                    el12 = quiniela.getJSONObject(5).getString("el12");
                    el13 = quiniela.getJSONObject(6).getString("el13");
                    el14 = quiniela.getJSONObject(7).getString("el14");
                    el15 = quiniela.getJSONObject(8).getString("el15");

                } else {
                    return resultado;
                }
                JSONArray partidos = quiniela.getJSONObject(i).getJSONArray("partit");
                for (int j = 0; j < partidos.length(); j++) {
                    JSONObject partido = partidos.getJSONObject(j);
                    resultado[cont] = partido.getString("sig");
                    cont = (cont + 1) % resultado.length;
                }
            }
            return  resultado;

        }catch (Exception e){
            throw new ExcepcionApuestas("Error en el fichero de apuestas");
        }

    }

    //metodo que se llama despues de obtener los resultados
    public  static Resultado analisisResultados(File ficheroApuestas,File premios,boolean xml,String[] resultados) throws IOException, XmlPullParserException, ExcepcionApuestas, ExcepcionEscritura {

        String lineaLeidaApuesta;//la linea con la apuesta
        int cantAciertoslinea=0;
        BufferedReader bf=null;
        result = new Resultado(0,0,0,0,0,0);

        String[] resultadosJornada=resultados;

        try{
            Apuesta atmp=null;
            if(resultados.length!=15){
                throw new ExcepcionFicheroResultados("fichero de resultados mal formado");
            }
         bf= new BufferedReader(new InputStreamReader(new FileInputStream(ficheroApuestas)));

            while ((lineaLeidaApuesta=bf.readLine())!=null){
                    lineaLeidaApuesta=lineaLeidaApuesta.trim();
                cantAciertoslinea=cantidadAciertos(resultadosJornada,lineaLeidaApuesta.toCharArray());

                switch (cantAciertoslinea){
                    case 10:
                        result.incCant10();
                        atmp=new Apuesta(lineaLeidaApuesta,cantAciertoslinea,el10);
                        break;
                    case 11:
                        result.incCant11();
                        atmp=new Apuesta(lineaLeidaApuesta,cantAciertoslinea,el11);
                        break;
                    case 12:
                        result.incCant12();
                        atmp=new Apuesta(lineaLeidaApuesta,cantAciertoslinea,el12);
                        break;
                    case 13:
                        result.incCant13();
                        atmp=new Apuesta(lineaLeidaApuesta,cantAciertoslinea,el13);
                        break;
                    case 14:
                        result.incCant14();
                        atmp=new Apuesta(lineaLeidaApuesta,cantAciertoslinea,el14);
                        break;
                    case 15:
                        result.incCant15();
                        atmp=new Apuesta(lineaLeidaApuesta,cantAciertoslinea,el15);
                        break;
                }
                if (cantAciertoslinea>=10)
                    apuestasPremiadas.add(atmp);
            }
        }catch (Exception e){
            throw new ExcepcionApuestas("Error en el fichero de apuestas");
        }
        finally {
            if(bf!=null){
                bf.close();
            }
        escrituraFichero(premios,xml);
        return result;
        }
    }

    public static int cantidadAciertos(String[] res,char[] apuesta){
        int cantAciertos=0;
        for (int i=0;i<res.length;i++){
            if(i<=13) {
                if (res[i].equalsIgnoreCase(String.valueOf(apuesta[i]))) {
                    cantAciertos++;
                }
            }else if((res[i].equalsIgnoreCase(String.valueOf((apuesta[i])).concat(String.valueOf(apuesta[i+1])))))
                cantAciertos++;
        }

        return cantAciertos;
    }

    private static void escrituraFichero(File fichero,boolean xml) throws ExcepcionEscritura {
        if(xml){
            createXML(fichero);
        }else{
            createJson(fichero);
        }
    }

    private static void createJson( File fichero) throws ExcepcionEscritura {
        OutputStreamWriter fout=null;
        try{
            fout= new FileWriter(fichero);
            JSONObject premiados,apuesta,objeto;
            JSONArray lista;

            //creacion json
            premiados=new JSONObject();
            premiados.put("10aciertos",result.getCant10());
            premiados.put("11aciertos",result.getCant11());
            premiados.put("12aciertos",result.getCant12());
            premiados.put("13aciertos",result.getCant13());
            premiados.put("14aciertos",result.getCant14());
            premiados.put("15aciertos",result.getCant15());

            lista=new JSONArray();

            for(Apuesta a : apuestasPremiadas){
                apuesta= new JSONObject();
                apuesta.put("aciertos",a.getCantAciertos());
                apuesta.put("apuesta",a.getApuesta());
                apuesta.put("premio",a.getPremio());
                lista.put(apuesta);
            }
            premiados.put("apuestas",lista);

            objeto= new JSONObject();
            objeto.put("premiados",premiados);

            fout.write(objeto.toString(4));

        }catch (Exception e){
            throw new ExcepcionEscritura("error al crear el fichero de premios");
        }
        finally {
            if(fout!=null){
                try {
                    fout.flush();
                    fout.close();
                } catch (IOException e) {}
            }
        }

    }
    private static void createXML(File fichero) throws ExcepcionEscritura {
        FileOutputStream fout=null;
        XmlSerializer serializer = null;
        try{
           fout= new FileOutputStream(fichero);
            serializer=Xml.newSerializer();
            serializer.setOutput(fout, "UTF-8");
            serializer.startDocument(null,true);
            serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);//tabulacion
            serializer.startTag(null,"premiados");
            //aciertos cantidad
            serializer.startTag(null,"el10aciertos");
            serializer.text(String.valueOf(result.getCant10()));
            serializer.endTag(null,"el10aciertos");

            serializer.startTag(null,"el11aciertos");
            serializer.text(String.valueOf(result.getCant11()));
            serializer.endTag(null,"el11aciertos");

            serializer.startTag(null,"el12aciertos");
            serializer.text(String.valueOf(result.getCant12()));
            serializer.endTag(null,"el12aciertos");

            serializer.startTag(null,"el13aciertos");
            serializer.text(String.valueOf(result.getCant13()));
            serializer.endTag(null,"el13aciertos");

            serializer.startTag(null,"el14aciertos");
            serializer.text(String.valueOf(result.getCant14()));
            serializer.endTag(null,"el14aciertos");

            serializer.startTag(null,"el15aciertos");
            serializer.text(String.valueOf(result.getCant15()));
            serializer.endTag(null,"el15aciertos");

            serializer.startTag(null,"apuestas_premiadas");
            for(Apuesta a : apuestasPremiadas){
                serializer.startTag(null,"apuesta");
                serializer.attribute(null,"aciertos",String.valueOf(a.getCantAciertos()));
                serializer.attribute(null,"premio",String.valueOf(a.getPremio()));
                serializer.text(a.apuesta);
                serializer.endTag(null,"apuesta");
            }
            serializer.endTag(null,"apuestas_premiadas");
            serializer.endTag(null,"premiados");
            serializer.endDocument();

        }catch (Exception e){
            throw new ExcepcionEscritura("error al crear el fichero de premios");
        }
        finally {
            try {
                if(serializer!=null)
                    serializer.flush();
                if(fout!=null)
                   fout.close();

                }
            catch (IOException e) {}
        }
    }

}

