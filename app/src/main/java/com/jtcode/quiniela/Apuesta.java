package com.jtcode.quiniela;


public class Apuesta {
    String apuesta;
    String premio;
    int cantAciertos;

    public Apuesta(String apuesta, int cantAciertos,String premio) {
        this.premio=premio;
        this.apuesta = apuesta;
        this.cantAciertos = cantAciertos;
    }

    public String getPremio(){return  premio;}

    public String getApuesta() {
        return apuesta;
    }

    public void setApuesta(String apuesta) {
        this.apuesta = apuesta;
    }

    public int getCantAciertos() {
        return cantAciertos;
    }

    public void setCantAciertos(int cantAciertos) {
        this.cantAciertos = cantAciertos;
    }


}
