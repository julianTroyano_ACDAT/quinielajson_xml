package com.jtcode.quiniela;


public class Resultado {

    int cant10;
    int cant11;
    int cant12;
    int cant13;
    int cant14;
    int cant15;

    public Resultado(int cant10, int cant11, int cant12, int cant13, int cant14, int cant15) {
        this.cant10 = cant10;
        this.cant11 = cant11;
        this.cant12 = cant12;
        this.cant13 = cant13;
        this.cant14 = cant14;
        this.cant15 = cant15;
    }

    public int getCant15() {
        return cant15;
    }

    public void incCant15() {
        this.cant15++;
    }

    public int getCant10() {
        return cant10;
    }

    public void incCant10() {
        this.cant10 ++;
    }

    public int getCant11() {
        return cant11;
    }

    public void incCant11() {
        this.cant11++;
    }

    public int getCant12() {
        return cant12;
    }

    public void incCant12() {
        this.cant12 ++;
    }

    public int getCant13() {
        return cant13;
    }

    public void incCant13( ) {
        this.cant13 ++;
    }

    public int getCant14() {
        return cant14;
    }

    public void incCant14() {
        this.cant14++;
    }
}
