package com.jtcode.quiniela;

public class ExcepcionApuestas extends Exception{
    public ExcepcionApuestas(String mens){
        super(mens);
    }
}

class ExcepcionEscritura extends Exception{
    public ExcepcionEscritura(String mens){super(mens);}
}

class ExcepcionFicheroResultados extends Exception{
    public ExcepcionFicheroResultados(String message) {super(message);}
}

class ExceotionFicherosDescargados extends Exception{
    public ExceotionFicherosDescargados(String message) {
        super(message);
    }
}

