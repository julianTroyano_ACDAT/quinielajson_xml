package com.jtcode.quiniela;


import android.os.Environment;

public interface Constantes {

    String RESXMLURL="";
    //String RESJSONURL="https://acdat0tests.000webhostapp.com/descarga/resultados.json";

    //String URLAPUESTA="https://acdat0tests.000webhostapp.com/descarga/apuestas.txt";
    String URLAPUESTA="";

    //String URLSERVGUARDAR="https://acdat0tests.000webhostapp.com/uploadP.php";
    String URLSERVGUARDAR="http://alumno.club/superior/julian/upload.php";

    //local
    String LOCALSDDIR= Environment.getExternalStorageDirectory().getAbsolutePath();
}
