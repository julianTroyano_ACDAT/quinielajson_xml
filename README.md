# App que descarga de una ruta seleccionada los datos de la ultima jornada terminada de la quiniela, el usuario indica en que formato quiere que se guarde el archivo del premio y tambien en que formato se va a analizar, han de tener el mismo formato el fichero con los resultados y el formato seleccionado en los radiobuttons. #

```
#!php
<?php
    $file_path = "upload/"; 
    $file_path = $file_path . basename( $_FILES['premios']['name']);
move_uploaded_file($_FILES["premios"]["tmp_name"], $file_path);
 ?>
```
# Al pulsar en el boton calcular descarga ambos ficheros y el resultado lo almacena en la memoria local y ademas sube el fichero con las apuestas premiadas al servidor

# El resultado de las apuestas se almacenan en formato xml con la siguiente jerarquía

```
#!xml

<premiados>
  <el10aciertos>0</el10aciertos>
  <el11aciertos>0</el11aciertos>
  <el12aciertos>0</el12aciertos>
  <el13aciertos>0</el13aciertos>
  <el14aciertos>0</el14aciertos>
  <el15aciertos>0</el15aciertos>

    <apuestas_premiadas>
      <apuesta aciertos="nAciertos" premio="Premio">Apuesta</apuesta>
    </apuestas_premiadas>

</premiados>
```

# En el caso de ser JSON el formato es:

```
#!json
{
    "premiados": {
        "10aciertos": 0,
        "11aciertos": 0,
        "12aciertos": 0,
        "13aciertos": 0,
        "14aciertos": 0,
        "15aciertos": 0,
        "apuestas": [
            {
                "aciertos": nAciertos,
                "apuesta": "apuesta",
                "premio": "premio"
            }
        ]
    }
}

```

## **la direccion del fichero de los resultados en funcion de su extensión tendrán la direccion de:
## XML: http://alumno.club/superior/julian/upload/premios.xml
## JSON http://alumno.club/superior/julian/upload/premios.json

## los fichero de descarga se pueden encontrar en:
http://alumno.club/superior/julian/descargas/